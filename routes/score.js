const express = require('express')
const router = express.Router()
const scoreController = require('../controllers/score')

router.post('/', (req, res) => {
    scoreController.saveScore(req.body).then(result => res.send(result))
})


router.get('/', (req, res) => {

    scoreController.getAllScores().then(result => res.send(result))
})

module.exports = router