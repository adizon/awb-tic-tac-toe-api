const mongoose = require('mongoose')
mongoose.set('useFindAndModify', false);

const scoreSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name is required']
    },
    score: {
        type: Number,
        default: 0
    }

})

module.exports = mongoose.model('scores', scoreSchema)