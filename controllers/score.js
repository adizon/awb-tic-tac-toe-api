const Score = require('../models/scores')
const dotenv = require('dotenv').config();

module.exports.saveScore = (params) => {
    return Score.find({name: params.name})
    .then((score,err) => {
        if (err) return false
        if (score.length === 0) {
            const score = new Score({
                name: params.name,
                score: params.score
            })

            return score.save()
            .then((newScore, err) => {
                if (err) {
                    return false
                } else {
                    return true
                }

            })
        } else {
            score[0].score += params.score;
            return score[0].save()
            .then((newScore, saveRrr) => {
                return (err) ? false : true
        })
        }
    })
}

module.exports.getAllScores = () => {
    return Score.find({}).sort({score: -1})
    .then(score => score)
}