const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv').config();

const app = express();
const port = process.env.PORT;

let corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200   // default
}

mongoose.connect(process.env.DB_MONGODB, { useNewUrlParser: true, useUnifiedTopology: true })

let db = mongoose.connection

db.on('error', () => { console.log('oops! something went wrong with our MongoDB connection') });

db.once('open', () => { console.log('connected to database') })

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.use(cors(corsOptions))

// import routes
const scoreRoutes = require('./routes/score')

// assign the imported route to their respective endpoints
app.use('/api/scores', scoreRoutes)

app.listen(port, () => {
    console.log("API is now online")
})